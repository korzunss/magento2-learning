<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Setup;

use \Magento\Cms\Model\BlockFactory;
use \Magento\Eav\Setup\EavSetupFactory;
use \Magento\Framework\Setup\ModuleDataSetupInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\UpgradeDataInterface;
/**
 * UpgradeData class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class UpgradeData implements UpgradeDataInterface
{
    const TABLE_NAME = 'magecom_items';
    protected $_blockFactory;
    protected $_eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        BlockFactory $blockFactory,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->_blockFactory    = $blockFactory;
        $this->_eavSetupFactory  = $eavSetupFactory;
    }
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            if ($setup->tableExists(self::TABLE_NAME)) {
                $this->_setFirstData($setup);
            }
        }

        if (version_compare($context->getVersion(), '0.0.3', '<')) {
            if ($setup->tableExists(self::TABLE_NAME)) {
                $this->_clearData($setup);
            }
        }

        if (version_compare($context->getVersion(), '0.0.4', '<')) {
            $this->_updateHomepage($setup);
        }

        if (version_compare($context->getVersion(), '0.0.5', '<')) {
            $this->_assignedNewProduct($setup);
        }
    }

    protected function _setFirstData(ModuleDataSetupInterface $setup)
    {
        $setup->getConnection()->insertArray(
            $setup->getTable(self::TABLE_NAME),
            ['title', 'content', 'url_key'],
            [
                ['first item', 'Content for first item', 'first'],
                ['second item', 'Content for second item', 'second'],
                ['third item', 'Content for third item', 'third']
            ]
        );
    }

    protected function _clearData(ModuleDataSetupInterface $setup) {
        $setup->getConnection()->truncateTable(self::TABLE_NAME);
    }

    protected function _updateHomepage(ModuleDataSetupInterface $setup)
    {
        $block = $this->_blockFactory->create()->load('home-page-block');
        if ($block->getId()) {
            $content = $block->getContent();
            $content .= '<p>{{widget type="Magecom\Learning\Block\Widget\Learning" text="Custom TEXT"}}</p>';
            $block->setData('content', $content)->save();
        }
    }

    protected function _assignedNewProduct(ModuleDataSetupInterface $setup)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);

        $fieldList = [
            'price',
            'special_price',
            'special_from_date',
            'special_to_date',
            'minimal_price',
            'cost',
            'tier_price',
            'weight',
        ];

        foreach ($fieldList as $field) {
            $applyTo = explode(
                ',',
                $eavSetup->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $field, 'apply_to')
            );
            if (!in_array(\Magecom\Learning\Model\Product\Type::TYPE_ID, $applyTo)) {
                $applyTo[] = \Magecom\Learning\Model\Product\Type::TYPE_ID;
                $eavSetup->updateAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    $field,
                    'apply_to',
                    implode(',', $applyTo)
                );
            }
        }
    }
}
