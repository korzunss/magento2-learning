<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Setup;

use \Magento\Framework\DB\Ddl\Table;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\Setup\UpgradeSchemaInterface;
/**
 * UpgradeSchema class
 *
 * @category    Magecom
 * @package     Magecom_Learing
 * @author      Magecom
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const TABLE_NAME = 'magecom_items';

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            if (!$setup->getConnection()->isTableExists(self::TABLE_NAME)) {
                $this->_createTable($setup);
            }
        }

        if (version_compare($context->getVersion(), '0.0.3', '<')) {
            if ($setup->getConnection()->isTableExists(self::TABLE_NAME)) {
                $this->_addStatusField($setup);
            }
        }

        $setup->endSetup();
    }

    protected function _createTable(SchemaSetupInterface $setup)
    {
        $table = $setup->getConnection()->newTable(
            $setup->getTable(self::TABLE_NAME)
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Title'
        )->addColumn(
            'content',
            Table::TYPE_TEXT,
            '1M',
            ['nullable' => false],
            'Content'
        )->addColumn(
            'url_key',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'URL Key'
        )->addColumn(
            'creation_time',
            Table::TYPE_TIMESTAMP,
            null,
            [ 'nullable' => false, 'default' => Table::TIMESTAMP_INIT ],
            'Post Creation Time'
        )->addColumn(
            'update_time',
            Table::TYPE_TIMESTAMP,
            null,
            [ 'nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE ],
            'Post Update Time'
        )->setComment(
            'Magecom Items Table'
        );
        $setup->getConnection()->createTable($table);
    }

    protected function _addStatusField(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable(self::TABLE_NAME),
            'status',
            [
                'type'      => Table::TYPE_SMALLINT,
                'unsigned'  => false,
                'nullable'  => false,
                'comment'   => 'Custom status'
            ]
        );
    }
}
