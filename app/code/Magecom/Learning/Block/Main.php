<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Block;

use \Magecom\Learning\Model\Cache\Type;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
/**
 * Main class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Main extends Template
{
    protected $_cache;
    protected $_cacheTags;
    protected $_cacheLifetime;

    /**
     * Main constructor.
     * @param Context $context
     * @param Type $cache
     * @param array $data
     * @param array $cacheTags
     * @param null $cacheLifetime
     */
    public function __construct(
        Context $context,
        Type $cache,
        array $data = [],
        array $cacheTags = [],
        $cacheLifetime = null
    )
    {
        $this->_cache           = $cache;
        $this->_cacheTags       = $cacheTags;
        $this->_cacheLifetime   = $cacheLifetime;
        parent::__construct($context, $data);
    }

    /**
     * Render html block
     *
     * @return string
     */
    protected function _toHtml()
    {
        $cacheId   = $this->_session->getSessionId();
        $html      = '';
        $cacheHtml = $this->_cache->load($cacheId);

        if ($cacheHtml) {
            return $cacheHtml;
        } else {
            if (!$this->getTemplate()) {
                return $html;
            }
            $html = $this->fetchView($this->getTemplateFile());
            $this->_cache->save($html, $cacheId, $this->_cacheTags, $this->_cacheLifetime);

            return $html;
        }
    }
}
