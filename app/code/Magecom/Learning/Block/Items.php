<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Block;

use Magecom\Learning\Model\ItemsFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
/**
 * Index class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Items extends Template
{
    protected $_itemCollection;
    protected $_itemsFactory;

    /**
     * Items constructor.
     * @param Context $context
     * @param ItemsFactory $itemsFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        ItemsFactory $itemsFactory,
        array $data = []
    )
    {
        $this->_itemsFactory = $itemsFactory;
        parent::__construct($context, $data);
    }

    public function getItemsCollection()
    {
        if (!$this->_itemCollection) {
            $this->_itemCollection = $this->_itemsFactory->create()->getCollection();
        }

        return $this->_itemCollection;
    }

    /**
     * Format date in short format
     *
     * @param string $date
     * @return string
     */
    public function dateFormat($date)
    {
        return $this->formatDate($date, \IntlDateFormatter::FULL);
    }
}
