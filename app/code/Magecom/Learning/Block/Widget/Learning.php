<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Block\Widget;

use \Magento\Framework\View\Element\Template;
use \Magento\Widget\Block\BlockInterface;
/**
 * Learning class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */

class Learning extends Template implements BlockInterface
{
    /**
     * Render widget HTML block
     *
     * @return $this
     */
    public function _toHtml()
    {
        $this->setTemplate('Magecom_Learning::widget/homepage.phtml');

        return parent::_toHtml();
    }
}
