<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

/**
 * Index class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Index extends Action
{
    protected $_resultPageFactory;
    protected $_logger;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        LoggerInterface $logger
    )
    {
        $this->_resultPageFactory   = $resultPageFactory;
        $this->_logger              = $logger;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_addInfoToLog();
        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
    }

    protected function _addInfoToLog()
    {
        $moduleName = $this->getRequest()->getModuleName();
        $controller = $this->getRequest()->getControllerName();
        $action     = $this->getRequest()->getActionName();

        $this->_logger->info($moduleName . '; ' . $controller . '; ' . $action);
        $this->_logger->notice($moduleName . '; ' . $controller . '; ' . $action);
        $this->_logger->debug($moduleName . '; ' . $controller . '; ' . $action);
        $this->_logger->warning($moduleName . '; ' . $controller . '; ' . $action);
        $this->_logger->error($moduleName . '; ' . $controller . '; ' . $action);
        $this->_logger->critical($moduleName . '; ' . $controller . '; ' . $action);
        $this->_logger->alert($moduleName . '; ' . $controller . '; ' . $action);
        $this->_logger->emergency($moduleName . '; ' . $controller . '; ' . $action);
    }
}
