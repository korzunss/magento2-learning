<?php
/**
 *
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Controller;

use \Magento\Framework\App\ActionFactory;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\App\RouterInterface;
use \Magento\Framework\App\RequestInterface;
/**
 * Router class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Router implements RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $_actionFactory;

    /**
     * Response
     *
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @param \Magento\Framework\App\ActionFactory $actionFactory
     * @param \Magento\Framework\App\ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response
    ) {
        $this->_actionFactory   = $actionFactory;
        $this->_response        = $response;
    }

    /**
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ActionInterface|void
     */
    public function match(RequestInterface $request)
    {
        $parts = explode('-', trim($request->getPathInfo(), '/'));
        if (count($parts) >= 3) {
            if(strpos($parts[0], 'learning') !== false && !$request->getModuleName()) {
                $request->setModuleName('learning')
                    ->setControllerName($parts[1])->setActionName($parts[2]);

                return $this->_actionFactory->create(
                    'Magento\Framework\App\Action\Forward',
                    ['request' => $request]
                );
            }
        }

        return;
    }
}