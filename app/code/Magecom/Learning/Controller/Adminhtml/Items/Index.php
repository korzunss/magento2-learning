<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Controller\Adminhtml\Items;

use \Magento\Backend\App\Action;
use \Magento\Backend\App\Action\Context;
use \Magento\Framework\Profiler;
use \Magento\Framework\View\Result\PageFactory;
/**
 * Index class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Index extends Action
{
    const ADMIN_RESOURCE = 'Magecom_Learning::items';

    protected $_resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        Profiler::start('profiler:magecom_learning');

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magecom_Learning::learning_items');
        $resultPage->addBreadcrumb(__('Magecom Learning Items'), __('Magecom Learning Items'));
        $resultPage->getConfig()->getTitle()->prepend(__('Magecom Learning Items'));

        Profiler::stop('profiler:magecom_learning');
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magecom_Learning::items_grid');
    }
}
