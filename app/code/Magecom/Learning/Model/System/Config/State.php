<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Model\System\Config;

use Magento\Framework\Data\OptionSourceInterface;
/**
 * State class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class State implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $_options;

    /**
     * Get Grid row status type labels array.
     * @return array
     */
    public function getOptionArray()
    {
        if ($this->_options === null) {
            $this->_options = [
                [
                    'value' => 0,
                    'label' => __('Disabled'),
                ],
                [
                    'value' => 1,
                    'label' => __('Enabled'),
                ],
                [
                    'value' => 2,
                    'label' => __('Archive'),
                ]
            ];
        }

        return $this->_options;
    }

    public function toOptionArray()
    {
        return $this->getOptionArray();
    }
}
