<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Model\Cache;

use \Magento\Framework\App\Cache\Type\FrontendPool;
use \Magento\Framework\Cache\Frontend\Decorator\TagScope;
/**
 * Type class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Type extends TagScope
{
    /**
     * Type Identifier
     */
    const TYPE_IDENTIFIER   = 'magecom_learning_cache_tag';

    /**
     *  Cache Tag
     */
    const CACHE_TAG         = 'MAGECOM_LEARNING_CACHE_TAG';

    /**
     * Type constructor.
     * @param FrontendPool $frontend
     */
    public function __construct(FrontendPool $frontend)
    {
        parent::__construct($frontend->get(self::TYPE_IDENTIFIER), self::CACHE_TAG);
    }
}
