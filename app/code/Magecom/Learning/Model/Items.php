<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Magecom\Learning\Model;

use \Magecom\Learning\Api\Data\ItemsInterface;
use \Magento\Framework\DataObject\IdentityInterface;
use \Magento\Framework\Model\AbstractModel;
/**
 * Items class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Items extends AbstractModel implements
    IdentityInterface,
    ItemsInterface
{
    const CACHE_TAG = 'magecom_items';

    protected function _construct()
    {
        $this->_init('Magecom\Learning\Model\ResourceModel\Items');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Item title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_getData(self::TITLE);
    }

    /**
     * Set item title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Item Content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->_getData(self::CONTENT);
    }

    /**
     * Set item content
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Item url key
     *
     * @return string
     */
    public function getUrlKey()
    {
        return $this->_getData(self::URL_KEY);
    }

    /**
     * Set item url key
     *
     * @param string $urlKey
     * @return $this
     */
    public function setUrlKey($urlKey)
    {
        return $this->setData(self::URL_KEY, $urlKey);
    }

    /**
     * Item creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set item created date
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreationTime($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Item update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->_getData(self::UPDATE_AT);
    }

    /**
     * Set item update date
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdateTime($updatedAt)
    {
        return $this->setData(self::UPDATE_AT, $updatedAt);
    }
}
