<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Plugin\Model;

use \Magento\Catalog\Model\Product as CoreProduct;
use \Magento\Catalog\Model\Product\Attribute\Source\Status;
/**
 * Product class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Product
{
    public function aroundGetStatus(CoreProduct $subject, callable $proceed)
    {
        return Status::STATUS_ENABLED;
    }

    /**
     * Add text to product name
     *
     * @param CoreProduct $subject
     * @param $result
     * @return string
     */
    public function afterGetName(CoreProduct $subject, $result)
    {
        return $result . ' ' . __('low price');
    }
}
