<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Plugin;

use \Magento\Framework\Data\Tree\Node;
use \Magento\Theme\Block\Html\Topmenu as CoreTopmenu;
/**
 * Topmenu class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Topmenu
{
    /**
     * Add a non-category link to the navigation links
     *
     * @param CoreTopmenu $subject
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @param int $limit
     */
    public function beforeGetHtml(
        CoreTopmenu $subject,
        $outermostClass = '',
        $childrenWrapClass = '',
        $limit = 0
    )
    {
        $tree = $subject->getMenu()->getTree();
        $requestString = 'contact';
        $data = [
            'name'      => __('Contact us'),
            'id'        => 'topmenu-learning-contact',
            'url'       => $subject->getUrl($requestString),
            'is_active' => ($subject->getRequest()->getRequestString() == '/' . $requestString . '/') ? 1 : 0
        ];
        $node = new Node($data, 'id', $tree);

        $subject->getMenu()->addChild($node);
    }
}
