<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Magecom\Learning\Api\Data;
/**
 * ItemsInterface interface
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
interface ItemsInterface
{

    const ID = 'id';
    const TITLE = 'title';
    const CONTENT = 'content';
    const URL_KEY = 'url_key';
    const CREATED_AT = 'creation_time';
    const UPDATE_AT = 'update_time';
    /**
     * Item id
     *
     * @return int|null
     */
    public function getId();

    public function setId($id);

    /**
     * Item title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set item title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Item Content
     *
     * @return string
     */
    public function getContent();

    /**
     * Set item content
     *
     * @param string $content
     * @return $this
     */
    public function setContent($content);

    /**
     * Item url key
     *
     * @return string
     */
    public function getUrlKey();

    /**
     * Set item url key
     *
     * @param string $urlKey
     * @return $this
     */
    public function setUrlKey($urlKey);

    /**
     * Item creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Set item created date
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreationTime($createdAt);

    /**
     * Item update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Set item update date
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdateTime($updatedAt);
}
