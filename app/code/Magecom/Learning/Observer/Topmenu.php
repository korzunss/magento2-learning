<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @copyright   Copyright (c) 2017 Magecom, Inc. (http://www.magecom.net)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Observer;

use \Magento\Framework\App\RequestInterface;
use \Magento\Framework\Data\Tree\Node;
use \Magento\Framework\Event\Observer as EventObserver;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\UrlInterface;
/**
 * Topmenu class
 *
 * @category    Magecom
 * @package     Magecom_Learning
 * @author      Magecom
 */
class Topmenu implements ObserverInterface
{
    protected $_request;
    protected $_url;

    /**
     * Topmenu constructor.
     * @param RequestInterface $request
     * @param UrlInterface $url
     */
    public function __construct(
        RequestInterface $request,
        UrlInterface $url
    )
    {
        $this->_request     = $request;
        $this->_url         = $url;
    }

    /**
     * Add a non-category link to the navigation links
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $menu = $observer->getMenu();
        $tree = $menu->getTree();
        $requestString = 'about-us';
        $data = [
            'name'      => __('About us'),
            'id'        => 'topmenu-learning-about',
            'url'       => $this->_url->getUrl($requestString),
            'is_active' => ($this->_request->getRequestString() == '/' . $requestString . '/') ? 1 : 0
        ];

        $node = new Node($data, 'id', $tree, $menu);

        if ($menu->hasChildren()) {
            $nodes = $menu->getChildren()->getNodes();
            foreach ($nodes as $element) {
                $menu->removeChild($element);
            }

            $menu->addChild($node);
            foreach ($nodes as $element) {
                $menu->addChild($element);
            }
        } else {
            $menu->addChild($node);
        }

        return $this;
    }
}
